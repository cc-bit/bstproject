/* Christopher Chitwood
   CSC364
   HW 4
   3/30/20
   This program is a modification for a BST implantation */

import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.text.Text;

// Imported for shadedNodes. --Chris C
import java.util.ArrayList;

public class BTView extends Pane {
    private BST<Integer> tree = new BST<>();
    private double radius = 15; // Tree node radius
    private double vGap = 50; // Gap between successive levels in the tree

    // Needed to be used in methods for shading the circles --Chris C
    public ArrayList<BST.TreeNode<Integer>> shadedNodes = new ArrayList<>();

    BTView(BST<Integer> tree) {
        this.tree = tree;
        setStatus("Tree is empty");
    }

    public void setStatus(String msg) {
        this.getChildren().add(new Text(20, 20, msg));
    }

    public void displayTree() {
        this.getChildren().clear(); // Clear the pane
        if (tree.getRoot() != null) {
            // Display tree recursively
            displayTree(tree.getRoot(), getWidth() / 2, vGap,
                    getWidth() / 4);
        }
    }

    /** Display a subtree rooted at position (x, y) */
    private void displayTree(BST.TreeNode<Integer> root,
                             double x, double y, double hGap) {
        if (root.left != null) {
            // Draw a line to the left node
            getChildren().add(new Line(x - hGap, y + vGap, x, y));
            // Draw the left subtree recursively
            displayTree(root.left, x - hGap, y + vGap, hGap / 2);
        }

        if (root.right != null) {
            // Draw a line to the right node
            getChildren().add(new Line(x + hGap, y + vGap, x, y));
            // Draw the right subtree recursively
            displayTree(root.right, x + hGap, y + vGap, hGap / 2);
        }

        // Display a node
        Circle circle = new Circle(x, y, radius);
        circle.setFill(Color.WHITE);

        // Overwrite fill color with orange when the input key is one of the shaded nodes. --Chris C
        if (shadedNodes.contains(root))
            circle.setFill(Color.ORANGE);

        circle.setStroke(Color.BLACK);
        this.getChildren().addAll(circle,
                new Text(x - 4, y + 4, root.element.toString()));
    }

    // As recommended in the HW pdf, BTView will provide to BSTAnimation the path.  --Chris C
    public ArrayList<BST.TreeNode<Integer>> getPath(int key) {
        return tree.path(key);
    }

    // The following three methods are for shading nodes and resetting the shading. --Chris C
    public void shadeNodes(ArrayList<BST.TreeNode<Integer>> shadedNodes) {
        this.shadedNodes = shadedNodes;
    }
    public ArrayList<BST.TreeNode<Integer>> getShadedNodes() {
        return shadedNodes;
    }
    public void clearShadedNodes() {
        shadedNodes.clear();
    }
}
