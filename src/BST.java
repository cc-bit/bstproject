/* Christopher Chitwood
   CSC364
   HW 4
   3/30/20
   This program is a modification for a BST implantation */

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

public class BST<E extends Comparable<E>> implements Tree<E> {
    protected TreeNode<E> root;
    protected int size = 0;

    /* Create a default binary tree */
    public BST() {
    }

    /* Create a binary tree from an array of objects */
    public BST(E[] objects) {
        for (int i = 0; i < objects.length; i++)
            add(objects[i]);
    }

    @Override /* Returns true if the element is in the tree */
    public boolean search(E e) {
        TreeNode<E> current = root; // Start from the root

        while (current != null) {
            if (e.compareTo(current.element) < 0) {
                current = current.left;
            }
            else if (e.compareTo(current.element) > 0) {
                current = current.right;
            }
            else
                return true; // e is found
        }

        return false;
    }

    @Override /* Insert element o into the binary tree
     * Return true if the element is inserted successfully */
    public boolean insert(E e) {
        if (root == null)
            root = createNewNode(e); // Create a new root
            //root = new TreeNode<>(e);	// Question:  Why not do it this way?  It would work.
        else {
            // Locate the parent node
            TreeNode<E> parent = null;
            TreeNode<E> current = root;
            while (current != null)
                if (e.compareTo(current.element) < 0) {
                    parent = current;
                    current = current.left;
                }
                else if (e.compareTo(current.element) > 0) {
                    parent = current;
                    current = current.right;
                }
                else
                    return false; // Duplicate node not inserted

            // Create the new node and attach it to the parent node
            if (e.compareTo(parent.element) < 0)
                parent.left = createNewNode(e);
                //parent.left = new TreeNode<>(e);
            else
                parent.right = createNewNode(e);
        }

        size++;
        return true; // Element inserted successfully
    }

    protected TreeNode<E> createNewNode(E e) {  // Factory method design pattern
        return new TreeNode<>(e);
    }

    @Override /* Inorder traversal from the root */
    public void inorder() {
        inorder(root);
    }

    /* Inorder traversal from a subtree */
    protected void inorder(TreeNode<E> root) {
        if (root == null) return;
        inorder(root.left);
        System.out.print(root.element + " ");
        inorder(root.right);
    }

    //Beginning of Edit by Chris Chitwood.
    public java.util.List<E> inorderList() {
        ArrayList<E> list = new ArrayList<>();
        inorderList(root, list);
        return list;
    }

    // Inorder is left, root, right
    //Notes from class had inorderList(TreeNode root, List list) returning void,
    // but it makes more sense to me to return the list.  Could be a mistake in my notes.
    private List<E> inorderList(TreeNode<E> root, List<E> list) {
        if (root != null) {
            inorderList(root.left, list); // Add the root's left child node.
            list.add(root.element);   // Add the root node
            inorderList(root.right, list); // Add the root's right child node.
            return list;
        }
        return list;
    }

    public List<E> preorderList() {
        ArrayList<E> list = new ArrayList<>();
        preorderList(root, list);
        return list;
    }

    // Preorder is root, left, right
    private List<E> preorderList(TreeNode<E> root, List<E> list) {
        if (root != null) {
            list.add(root.element);
            preorderList(root.left, list);
            preorderList(root.right, list);
            return list;
        }
        return list;
    }

    public List<E> postorderList() {
        ArrayList<E> list = new ArrayList<>();
        postorderList(root, list);
        return list;
    }

    // Postorder is left, right, root.
    private List<E> postorderList(TreeNode<E> root, List<E> list) {
        if (root != null) {
            postorderList(root.left, list);
            postorderList(root.right, list);
            list.add(root.element);
            return list;
        }
        return list;
    }

    public List<E> breadthFirstOrderList() {
        List<E> list = new ArrayList<>();
        breadthFirstOrderList(root, list);
        return list;
    }

    /*
      Wikipedia pseudo code provided in HW

      levelorder(root)
        q = empty queue
        q.enqueue(root)
        while not q.empty do
          node := q.dequeue()
          visit(node)
          if node.left ≠ null then
            q.enqueue(node.left)
          if node.right ≠ null then
            q.enqueue(node.right)
     */

    private List<E> breadthFirstOrderList(TreeNode<E> root, List<E> list) {
        Queue<TreeNode<E>> q = new ArrayDeque<>() {};  // Make an empty queue
        q.add(root); // Equivalent to q.enqueue(root)
        while (!(q.isEmpty())) {  // Equivalent to while not q.empty
            TreeNode<E> node = q.poll();  // Equivalent to node := q.dequeue()
            list.add(node.element);  // visit(node)
            if (node.left != null)     //if node.left ≠ null then
                q.add(node.left);  // q.enqueue(node.left)
            if (node.right != null)  //// if node.right ≠ null then
                q.add(node.right); // q.enqueue(node.right)
        }
        return list;
    }

    public int height() {
        return height(root);
    }

    // Height of tree is length of path from root.
    private int height(TreeNode<E> root) {
        if (root==null) return -1;  // If there are no nodes, the height is -1.
        return 1 + Math.max(height(root.left), height(root.right));  // (Recursively)Return the largest path + 1.
    }

    //End of Edit by Chris Chitwood.



    @Override /* Postorder traversal from the root */
    public void postorder() {
        postorder(root);
    }

    /* Postorder traversal from a subtree */
    protected void postorder(TreeNode<E> root) {
        if (root == null) return;
        postorder(root.left);
        postorder(root.right);
        System.out.print(root.element + " ");
    }

    @Override /* Preorder traversal from the root */
    public void preorder() {
        preorder(root);
    }

    /* Preorder traversal from a subtree */
    protected void preorder(TreeNode<E> root) {
        if (root == null) return;
        System.out.print(root.element + " ");
        preorder(root.left);
        preorder(root.right);
    }

    /* This inner class is static, because it does not access
     any instance members defined in its outer class */
    public static class TreeNode<E> {
        protected E element;
        protected TreeNode<E> left;
        protected TreeNode<E> right;

        public TreeNode(E e) {
            element = e;
        }
    }

    @Override /* Get the number of nodes in the tree */
    public int getSize() {
        return size;
    }

    /* Returns the root of the tree */
    public TreeNode<E> getRoot() {
        return root;
    }

    /* Returns a path from the root leading to the specified element */
    public java.util.ArrayList<TreeNode<E>> path(E e) {
        java.util.ArrayList<TreeNode<E>> list =
                new java.util.ArrayList<>();
        TreeNode<E> current = root; // Start from the root

        while (current != null) {
            list.add(current); // Add the node to the list
            if (e.compareTo(current.element) < 0) {
                current = current.left;
            }
            else if (e.compareTo(current.element) > 0) {
                current = current.right;
            }
            else
                break;
        }

        return list; // Return an array list of nodes
    }

    @Override /* Delete an element from the binary tree.
     * Return true if the element is deleted successfully
     * Return false if the element is not in the tree */
    public boolean delete(E e) {
        // Locate the node to be deleted and also locate its parent node
        TreeNode<E> parent = null;
        TreeNode<E> current = root;
        while (current != null) {
            if (e.compareTo(current.element) < 0) {
                parent = current;
                current = current.left;
            }
            else if (e.compareTo(current.element) > 0) {
                parent = current;
                current = current.right;
            }
            else
                break; // Element is in the tree pointed at by current
        }

        if (current == null)
            return false; // Element is not in the tree

        // Case 1: current has no left child
        if (current.left == null) {
            // Connect the parent with the right child of the current node
            if (parent == null) {
                root = current.right;
            }
            else {
                if (e.compareTo(parent.element) < 0)
                    parent.left = current.right;
                else
                    parent.right = current.right;
            }
        }
        else {
            // Case 2: The current node has a left child
            // Locate the rightmost node in the left subtree of
            // the current node and also its parent
            TreeNode<E> parentOfRightMost = current;
            TreeNode<E> rightMost = current.left;

            while (rightMost.right != null) {
                parentOfRightMost = rightMost;
                rightMost = rightMost.right; // Keep going to the right
            }

            // Replace the element in current by the element in rightMost
            current.element = rightMost.element;

            // Eliminate rightmost node
            if (parentOfRightMost.right == rightMost)
                parentOfRightMost.right = rightMost.left;
            else
                // Special case: parentOfRightMost.left == rightMost, happens if parentOfRightMost is current
                parentOfRightMost.left = rightMost.left;
        }

        size--;
        return true; // Element deleted successfully
    }

    @Override /* Obtain an iterator. Use inorder. */
    public java.util.Iterator<E> iterator() {
        return new InorderIterator();
    }

    // Inner class InorderIterator
    private class InorderIterator implements java.util.Iterator<E> {
        // Store the elements in a list
        private java.util.ArrayList<E> list =
                new java.util.ArrayList<>();
        private int current = 0; // Index of next element in the iteration
        private boolean canRemove = false;

        public InorderIterator() {
            inorder(); // Traverse binary tree and store elements in list
        }

        /* Inorder traversal from the root*/
        private void inorder() {
            inorder(root);
        }

        /* Inorder traversal from a subtree */
        private void inorder(TreeNode<E> root) {
            if (root == null) return;
            inorder(root.left);
            this.list.add(root.element);
            inorder(root.right);
        }

        @Override /* More elements for traversing? */
        public boolean hasNext() {
            return current < list.size();
        }

        @Override /* Get the current element and move to the next */
        public E next() {
            if (hasNext())
                canRemove = true;
            else
                throw new java.util.NoSuchElementException();
            return list.get(current++);
        }

        @Override /* Remove the element most recently returned */
        public void remove() {
            if (!canRemove)
                throw new IllegalStateException();
            BST.this.delete(list.get(--current));
            InorderIterator.this.list.remove(current);
            canRemove = false;
        }
    }

    @Override /* Remove all elements from the tree */
    public void clear() {
        root = null;
        size = 0;
    }
}
