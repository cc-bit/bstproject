/* Christopher Chitwood
   CSC364
   HW 4
   3/30/20
   This program is a modification for a BST implantation */

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;

// Imported for path of shadedNodes --Chris C
import java.util.ArrayList;

public class BSTAnimation extends Application {
    @Override // Override the start method in the Application class
    public void start(Stage primaryStage) {
        BST<Integer> tree = new BST<>(); // Create a tree

        BorderPane pane = new BorderPane();
        BTView view = new BTView(tree); // Create a View
        pane.setCenter(view);

        TextField tfKey = new TextField();
        tfKey.setPrefColumnCount(3);
        tfKey.setAlignment(Pos.BASELINE_RIGHT);
        Button btInsert = new Button("Insert");
        Button btDelete = new Button("Delete");

        //Added by Chris Chitwood
        Button btInorder = new Button("Inorder");
        Button btPreorder = new Button("Preorder");
        Button btPostorder = new Button("Postorder");
        Button btBreadthFirst = new Button("Breadth-first");
        Button btHeight = new Button("Height");
        Button btSearch = new Button("Search");


        HBox hBox = new HBox(5);

        //Edited by Chris Chitwood to add buttons to JavaFX window.
        hBox.getChildren().addAll(new Label("Enter a key: "),
                tfKey, btInsert, btDelete, btSearch,btInorder, btPreorder,
                btPostorder, btBreadthFirst, btHeight);

        hBox.setAlignment(Pos.CENTER);
        pane.setBottom(hBox);

        btInsert.setOnAction(e -> {
            int key = Integer.parseInt(tfKey.getText());
            if (tree.search(key)) { // key is in the tree already
                view.displayTree();   // Clears the old status message
                view.setStatus(key + " is already in the tree");
            }
            else {
                tree.insert(key); // Insert a new key
                view.displayTree();
                view.setStatus(key + " is inserted in the tree");
            }
        });

        btDelete.setOnAction(e -> {
            int key = Integer.parseInt(tfKey.getText());
            if (!tree.search(key)) { // key is not in the tree
                view.displayTree();    // Clears the old status message
                view.setStatus(key + " is not in the tree");
            }
            else {
                // Added by Chris Chitwood to clear the shaded nodes when deleting a node that's in the shaded path.
                ArrayList<BST.TreeNode<Integer>> path = view.getPath(key);
                if (view.getShadedNodes().contains(path.get(path.size() - 1)))
                    view.clearShadedNodes();

                tree.delete(key); // Delete a key
                view.displayTree();
                view.setStatus(key + " is deleted from the tree");
            }
        });

        // Added by Chris Chitwood
        btInorder.setOnAction(e -> {
            view.displayTree();
            view.setStatus("Inorder Traversal: " + tree.inorderList());
        });

        btPreorder.setOnAction(e ->{
            view.displayTree();
            view.setStatus("Preorder Traversal: " + tree.preorderList());
        });

        btPostorder.setOnAction(e -> {
            view.displayTree();
            view.setStatus("Postorder Traversal: " + tree.postorderList());
        });

        btBreadthFirst.setOnAction(e -> {
            view.displayTree();
            view.setStatus("Breadth-first Traversal: " + tree.breadthFirstOrderList());
        });

        btHeight.setOnAction(e -> {
            view.displayTree();
            view.setStatus("Tree Height: " + tree.height());
        });

        btSearch.setOnAction(e -> {
            int key = Integer.parseInt(tfKey.getText());
            view.shadeNodes(view.getPath(key));
            if (!tree.search(key)) {
                view.displayTree();
                view.setStatus(key + " is not in the tree.");
            } else {
                tree.search(key);
                view.displayTree();
                view.setStatus("Found " + key + " in the tree");
            }
        });

        // End of Edit by Chris Chitwood

        // Create a scene and place the pane in the stage
        Scene scene = new Scene(pane, 800, 300);
        primaryStage.setTitle("BSTAnimation"); // Set the stage title
        primaryStage.setScene(scene); // Place the scene in the stage
        primaryStage.show(); // Display the stage
    }

    /**
     * The main method is only needed for the IDE with limited
     * JavaFX support. Not needed for running from the command line.
     */
    public static void main(String[] args) {
        launch(args);
    }
}

